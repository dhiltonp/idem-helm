import asyncio
import sys
import unittest.mock as mock
from os import environ
from typing import Any
from typing import Dict
from typing import List

import dict_tools
import pop.hub
import pytest


# ================================================================================
# pop fixtures
# ================================================================================
@pytest.fixture(scope="module", autouse=True, name="acct_subs")
def acct_subs() -> List[str]:
    return ["helm"]


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    return "test_development_helm"


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    try:
        yield loop
    finally:
        loop.close()


@pytest.fixture(scope="module", name="hub")
def tpath_hub(code_dir, event_loop):
    """
    Add "idem_plugin" to the test path
    """
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        hub.pop.sub.add(dyne_name="idem")
        hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)
        hub.idem.RUNS = {"test": {}}
        yield hub


@pytest.fixture(scope="module", name="ctx")
async def integration_ctx(
    hub, acct_subs: List[str], acct_profile: str
) -> Dict[str, Any]:
    ctx = dict_tools.data.NamespaceDict(
        run_name="test", test=False, tag="fake_|-test_|-tag"
    )

    # Add the profile to the account
    if hub.OPT.acct.acct_file and hub.OPT.acct.acct_key:
        await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
        ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)
        if environ.get("KUBE_CONFIG_TEST_FILE"):
            ctx.acct.kube_config_path = environ.get("KUBE_CONFIG_TEST_FILE")
    else:
        hub.log.warning("No credentials found, considering below k8s account")
        ctx.acct = dict(
            kube_config_path="idem_repo/helm/idem-helm/kube-config",
            context="kubernetes-admin@kubernetes",
        )

    yield ctx
